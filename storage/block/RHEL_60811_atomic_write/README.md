# storage/block/RHEL_60811_atomic_write

Storage: enable block atomic write test

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```

Note: the feature not enable now, and this case not complete yet.
